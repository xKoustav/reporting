import { groq } from "next-sanity";
import { client } from "@/lib/sanity.client";
import HomeContainer from "@/components/HomeContainer";

const query = groq`
*[_type == 'category'] {
 ...
}`;

const queryReports = groq`
  *[_type == 'post'] {
    ...,
    author->,
    categories[]->
  } | order(_createdAt desc)
`;

export default async function Home() {
  const categories = await client.fetch(query);
  const reports = await client.fetch(queryReports);

  return <HomeContainer categories={categories} reports={reports} />;
}
