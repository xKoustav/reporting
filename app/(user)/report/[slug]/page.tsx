import { groq } from "next-sanity";
import { client } from "@/lib/sanity.client";
import Image from "next/image";
import urlFor from "@/lib/urlFor";
import { PortableText } from "@portabletext/react";
import { RichTextComponent } from "@/components/RichTextComponent";
import type { Report } from "common-types";
import { Share } from "@/components/Share";
import { Metadata, ResolvingMetadata } from "next";
import dynamic from "next/dynamic";
import { Suspense } from "react";

type Props = {
  params: {
    slug: string;
  };
};

export async function generateMetadata(
  { params: { slug } }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  const query = groq`
  *[_type == 'post' && slug.current == $slug][0] {
    ...,
    author->,
    categories[]->,
  }
`;
  const post: Report = await client.fetch(query, { slug });
  const myURL = urlFor(post.mainImage)?.url();
  const previousImages = (await parent).openGraph?.images || [];

  return {
    metadataBase: new URL("http://localhost:3000"),
    title: post?.title,
    description: post?.description,
    openGraph: {
      title: post?.title,
      description: post?.description,
      images: [myURL, ...previousImages]
    },
    twitter: {
      title: post?.title,
      description: post?.description,
      images: [myURL, ...previousImages]
    }
  };
}

// - ISR
// revalidate this page every 30 seconds
export const revalidate = 30;

export async function generateStaticParams() {
  const query = groq`
    *[_type == 'post'] {
      slug
    }
  `;
  const slugs: Report[] = await client.fetch(query);
  const slugRoutes = slugs.map((slug) => slug.slug.current);

  return slugRoutes.map((slug) => ({
    slug
  }));
}

async function PostPage({ params: { slug } }: Props) {
  const query = groq`
    *[_type == 'post' && slug.current == $slug][0] {
      ...,
      author->,
      categories[]->,
    }
  `;

  const post: Report = await client.fetch(query, { slug });

  return (
    <div>
      {post && (
        <>
          <article className="container mx-auto mt-12 flex flex-1 flex-col items-center justify-center px-10 pb-28">
            <section className="g:space-x-5  mb-10 pt-5">
              <h1 className="text-7xl  font-bold">{post.title}</h1>
              <div className="mt-4 flex flex-col justify-between gap-y-5 md:flex-row">
                <div className="mt-auto flex items-center justify-end space-x-2">
                  {post.categories.map((category) => (
                    <p
                      key={category._id}
                      className="mt-4 rounded-full bg-blue-400 px-3 py-1 text-sm font-semibold text-white"
                    >
                      {category.title}
                    </p>
                  ))}
                </div>
                <div>
                  <p className="text-lg font-bold">{post.author.name}</p>{" "}
                  <p className="font-normal">
                    {new Date(post.publishedAt).toLocaleDateString("en-US", {
                      day: "numeric",
                      month: "long",
                      year: "numeric"
                    })}
                  </p>
                </div>
              </div>
              <div className="flex justify-center">
                {post && post.mainImage && (
                  <div className="relative min-h-[50vh] min-w-[50vw]">
                    <Image
                      className="mx-auto object-cover object-center"
                      src={urlFor(post.mainImage)?.url()}
                      alt={post.author.name}
                      fill
                    ></Image>
                  </div>
                )}
              </div>
              <hr className="mb-4 mt-4" />
              <Suspense fallback={<p>Loading ...</p>}>
                <Share title={post.title} />
              </Suspense>
            </section>

            <PortableText
              value={post.body}
              components={RichTextComponent}
            ></PortableText>
          </article>
        </>
      )}
    </div>
  );
}
export default PostPage;
