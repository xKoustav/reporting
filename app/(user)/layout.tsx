import type { Metadata } from "next";
import "../globals.css";
import Header from "@/components/Header";

export const metadata: Metadata = {
  title: "Reporting | Rural Initiative Yi",
  description:
    "Access and download comprehensive reports to explore all the year's events on a single, user-friendly page.",
  icons: {
    icon: "https://static.wixstatic.com/media/802a3d_273b125e88874d5a853b3d4d469435bb%7Emv2.png/v1/fill/w_192%2Ch_192%2Clg_1%2Cusm_0.66_1.00_0.01/802a3d_273b125e88874d5a853b3d4d469435bb%7Emv2.png" // /public path
  }
};

export default function RootLayout({
  children
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <Header />
        {children}
      </body>
    </html>
  );
}
