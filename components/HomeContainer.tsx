"use client";
import bg from "../public/bg2.jpg";
import Welcome from "./Welcome";
import Reports from "./Reports";
import { useMemo, useRef, useState } from "react";
import { Category, Report } from "common-types";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";
const HomeContainer = ({
  categories,
  reports
}: {
  categories: Category[];
  reports: Report[];
}) => {
  const pdfRef = useRef<HTMLDivElement>(null);

  const [selected, setSelected] = useState<Category>({
    _id: null,
    title: "Select Vertical Name"
  });

  const filteredReports = useMemo(() => {
    let filtered = reports;

    if (selected && selected._id !== null) {
      filtered = reports.filter((info) =>
        info.categories.some(
          (member: Category) => member._id === String(selected._id)
        )
      );
    }

    return filtered;
  }, [reports, selected]);

  const handleGeneratePdf = async () => {
    const pdfElement = document.getElementById("print"); // Assuming 'print' is the ID of your container
    const items = pdfElement!.querySelectorAll("#print-item");

    const pdf = new jsPDF("p", "mm", "a4");

    for (let i = 0; i < items.length; i++) {
      const item = items[i];

      // Create a new page for each item, except for the first item
      if (i > 0) {
        pdf.addPage();
      }

      //@ts-ignore
      await html2canvas(item).then((canvas) => {
        let imgWidth = 208;
        let imgHeight = (canvas.height * imgWidth) / canvas.width;
        const imgData = canvas.toDataURL("img/png");

        // Add image to the PDF, position it at (0, 0), and set its dimensions
        pdf.addImage(imgData, "PNG", 0, 0, imgWidth, imgHeight);
      });
    }

    // Save the PDF with a specific name
    pdf.save("download.pdf");
  };

  return (
    <div className="mb-32 h-full min-h-screen">
      <div
        className="flex h-[50vh]  w-screen items-center justify-center bg-[rgba(0,0,0,0.3)] bg-center bg-blend-multiply"
        style={{ backgroundImage: `url(${bg.src})` }}
      >
        <Welcome
          categories={categories}
          selected={selected}
          setSelected={setSelected}
          handleGeneratePdf={() => handleGeneratePdf()}
        />
      </div>
      <Reports reports={filteredReports} pdfRef={pdfRef} />
    </div>
  );
};

export default HomeContainer;
