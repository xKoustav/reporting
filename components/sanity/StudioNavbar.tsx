import Link from "next/link";
import { ArrowUturnLeftIcon } from "@heroicons/react/24/solid";

function StudioNavbar(props: any) {
  return (
    <div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          padding: 15
        }}
      >
        <Link href="/" style={{ color: "white" }}>
          <ArrowUturnLeftIcon
            style={{ marginRight: 5, height: 24, width: 24 }}
            aria-hidden="true"
          />
          Go to Website
        </Link>
      </div>
      <>{props.renderDefault(props)}</>
    </div>
  );
}
export default StudioNavbar;
