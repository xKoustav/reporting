import Image from "next/image";
import brandLogo from "@/public/Yi.webp";

function Logo(props: any) {
  const { renderDefault, title } = props;
  return (
    <div style={{ display: "flex", alignItems: "center", marginRight: 10 }}>
      <Image
        style={{ objectFit: "cover" }}
        width={24}
        height={24}
        src={brandLogo}
        alt="brand logo"
      ></Image>
      {renderDefault && <>{renderDefault(props)}</>}
    </div>
  );
}
export default Logo;
