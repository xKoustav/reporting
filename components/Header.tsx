"use client";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import YI from "../public/Yi.webp";
import RI from "../public/RI.webp";
import CI from "../public/CI.webp";

function Header() {
  const pathname = usePathname();
  return (
    <>
      <header className="mb-4 flex h-[124px] w-full flex-col justify-between border-b border-gray-200 shadow-md">
        <div className="flex items-center justify-between md:mx-36">
          <div className="flex items-center py-2">
            <div className="mr-12">
              <Image src={YI} alt="YI LOGO" width={"78"} height={"63"} />
            </div>
            <div>
              {" "}
              <Image src={RI} alt="RI LOGO" width={"181"} height={"70"} />
            </div>
          </div>
          <div>
            {" "}
            <Image src={CI} alt="CI LOGO" width={"189"} height={"56"} />
          </div>
        </div>
        <div className="flex w-full items-center justify-center bg-gray-100 p-2">
          <div className="flex w-2/3">
            <ul className="flex cursor-pointer space-x-8">
              <li className="font-[300]">
                <a href="https://www.rikolkata.com/">Home</a>
              </li>
              <li className="font-[300]">
                <a href="https://www.rikolkata.com/about">About</a>
              </li>
              <li className="font-[300]">
                <a href="https://www.rikolkata.com/projects">Projects</a>
              </li>
              <li className="font-[300]">
                <a href="https://www.rikolkata.com/videos">Videos</a>
              </li>
              <li className="font-[300]">
                <a href="https://www.rikolkata.com/donate">Donate</a>
              </li>
              <NavItem href="/" label="Reporting 2024" currentPath={pathname} />
            </ul>
          </div>
        </div>
      </header>
    </>
  );
}
const NavItem = ({
  href,
  label,
  currentPath
}: {
  href: string;
  label: string;
  currentPath: string;
}) => {
  const isActive = href === currentPath;

  return (
    <li className="font-[300]">
      <Link href={href}>
        <p className={`text-black ${isActive ? "text-red-400" : ""}`}>
          {label}
        </p>
      </Link>
    </li>
  );
};
export default Header;
