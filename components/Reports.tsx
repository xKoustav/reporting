import { Report } from "common-types";
import Image from "next/image";

import { ArrowUpRightIcon } from "@heroicons/react/24/solid";
import ClientSideRoute from "./ClientSideRoute";
import urlFor from "@/lib/urlFor";
import { RefObject } from "react";

const Reports = ({
  reports,
  pdfRef
}: {
  reports: Report[];
  pdfRef: RefObject<HTMLDivElement>;
}) => {
  return (
    <div className="container mx-auto flex flex-1 flex-col items-center justify-center p-4">
      <div>
        <hr className="mb-10" />

        <div className="flex flex-col" ref={pdfRef} id="print">
          {/* Posts */}
          {reports.map((post) => (
            <div id="print-item" key={post._id} className="mb-8">
              <ClientSideRoute route={`/report/${post.slug.current}`}>
                <div className="group mt-5 flex cursor-pointer">
                  <div className="relative mx-4 w-1/3 ">
                    <Image
                      className="h-72  rounded-md"
                      src={urlFor(post.mainImage).url()}
                      alt={post.author.name}
                      width={500}
                      height={400}
                      objectFit="cover"
                    ></Image>

                    <div className="my-4  flex w-full items-center justify-between">
                      <div className="flex flex-col gap-y-2 md:flex-row md:gap-x-2">
                        {post.categories.map((category) => (
                          <div
                            key={category._id}
                            className="h-fit rounded-full bg-[#F7AB0A] px-3 py-1 text-center text-sm font-semibold text-black"
                          >
                            <p>{category.title}</p>
                          </div>
                        ))}
                      </div>
                      <p className="mr-8 transform-gpu subpixel-antialiased">
                        {new Date(post.publishedAt).toLocaleDateString(
                          "en-US",
                          {
                            day: "numeric",
                            month: "long",
                            year: "numeric"
                          }
                        )}
                      </p>
                    </div>
                  </div>
                  <div className="flex h-auto w-2/3 flex-col ">
                    <div>
                      <p className="mb-4 text-lg font-bold ">{post.title}</p>
                      <p className="line-clamp-8 w-2/3 text-gray-500">
                        {post.description}
                      </p>
                    </div>
                    <div>
                      <p className="mt-5 flex items-center font-bold group-hover:underline">
                        Read
                        <ArrowUpRightIcon className="ml-2 h-4 w-4"></ArrowUpRightIcon>
                      </p>
                    </div>
                  </div>
                </div>
              </ClientSideRoute>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Reports;
