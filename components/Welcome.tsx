import { Dispatch, Fragment, SetStateAction, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import {
  CheckIcon,
  ChevronUpDownIcon,
  XMarkIcon
} from "@heroicons/react/20/solid";
import { Category } from "common-types";

const Welcome = ({
  categories,
  selected,
  setSelected,
  handleGeneratePdf
}: {
  categories: Category[];
  selected: Category;
  setSelected: Dispatch<SetStateAction<any>>;
  handleGeneratePdf: () => void;
}) => {
  return (
    <div className="">
      <div className="flex h-auto w-[50vw] flex-col items-start justify-center rounded-lg bg-white p-20 shadow-md">
        <p className="text-4xl font-normal">Event Reporting 2024</p>
        <p className="mt-4 text-lg font-[200]">
          {`Access and download comprehensive reports to explore all the year's
    events on a single, user-friendly page.`}
        </p>
        <div className="mt-4 flex items-center">
          <div>
            <Listbox value={selected} onChange={setSelected}>
              <div className="relative mt-1">
                <Listbox.Button className="relative w-64 cursor-default rounded-lg border border-blue-300 bg-white py-4 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white/75 focus-visible:ring-offset-2 focus-visible:ring-offset-blue-300 sm:text-sm">
                  <span className="block truncate">{selected.title}</span>
                  <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                    <ChevronUpDownIcon
                      className="h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </span>
                </Listbox.Button>
                <Transition
                  as={Fragment}
                  leave="transition ease-in duration-100"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-md ring-1 ring-black/5 focus:outline-none sm:text-sm">
                    {categories?.map((person, personIdx) => (
                      <Listbox.Option
                        key={personIdx}
                        className={({ active }) =>
                          `relative cursor-default select-none py-2 pl-10 pr-4 ${
                            active
                              ? "bg-blue-100 text-blue-900"
                              : "text-gray-900"
                          }`
                        }
                        value={person}
                      >
                        {({ selected }) => (
                          <>
                            <span
                              className={`block truncate ${
                                selected ? "font-medium" : "font-normal"
                              }`}
                            >
                              {person.title}
                            </span>
                            {selected ? (
                              <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-blue-600">
                                <CheckIcon
                                  className="h-5 w-5"
                                  aria-hidden="true"
                                />
                              </span>
                            ) : null}
                          </>
                        )}
                      </Listbox.Option>
                    ))}
                  </Listbox.Options>
                </Transition>
              </div>
            </Listbox>
          </div>
          <div className="mx-8">
            <button
              onClick={() => handleGeneratePdf()}
              className="rounded-sm border border-blue-500 bg-blue-400 px-4 py-3 text-white shadow-md hover:rounded-2xl"
            >
              Download
            </button>
          </div>
          {selected._id !== null && (
            <div
              className="flex cursor-pointer items-center space-x-1 text-gray-400 hover:text-red-400"
              onClick={() =>
                setSelected({
                  _id: null,
                  title: "Select Vertical Name"
                })
              }
            >
              <p>Remove</p> <XMarkIcon className="h-5 w-5 " />{" "}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Welcome;
