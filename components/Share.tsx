"use client";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import {
  WhatsappShareButton,
  WhatsappIcon,
  TwitterIcon,
  TwitterShareButton,
  FacebookShareButton,
  FacebookIcon
} from "react-share";

export const Share = ({ title }: { title: string }) => {
  const pathname = usePathname();
  const [currentUrl, setCurrentUrl] = useState("");
  useEffect(() => {
    if (window) {
      setCurrentUrl(window.location.href);
    }
  }, []);

  return (
    <div className="flex items-center justify-end">
      {pathname && (
        <>
          <div className="mx-1">
            <WhatsappShareButton url={currentUrl}>
              <WhatsappIcon size={32} />
            </WhatsappShareButton>
          </div>
          <div className="mx-1">
            <TwitterShareButton url={currentUrl}>
              <TwitterIcon size={32} />
            </TwitterShareButton>
          </div>
          <div className="mx-1">
            <FacebookShareButton url={currentUrl}>
              <FacebookIcon size={32} />
            </FacebookShareButton>
          </div>
        </>
      )}
    </div>
  );
};
